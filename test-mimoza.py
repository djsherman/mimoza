import hamcrest
from prego import Task, TestCase, context as ctx, running

class Mimoza(TestCase):
    def test_netcat(self):
        ctx.port = 2000
        server = Task(desc='mimoza command line')
        cmd = server.command('python mimoza.py --help')
        server.assert_that(cmd.stdout.content,
                           hamcrest.contains_string('usage: mimoza.py'))
